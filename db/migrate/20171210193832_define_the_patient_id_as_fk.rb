class DefineThePatientIdAsFk < ActiveRecord::Migration[5.1]
  def up
    add_index :appointments, :patient_id
  end
  def down
    remove_index :appointments, :patient_id
  end
end
