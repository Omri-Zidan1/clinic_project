class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.string :date
      t.string :time
      t.string :clinic
      t.string :address
      t.integer :patient_id

      t.timestamps
    end
  end
end
