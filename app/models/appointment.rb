class Appointment < ApplicationRecord
  belongs_to :patient, optional: true
end
