json.extract! appointment, :id, :date, :time, :clinic, :address, :patient_id, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)
